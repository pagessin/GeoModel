On linux:

```
wget https://atlas-vp1.web.cern.ch/atlas-vp1/geometryfiles/geometry_atlas.db
```

On mac:

```
curl https://atlas-vp1.web.cern.ch/atlas-vp1/geometryfiles/geometry_atlas.db -o geometry_atlas.db
```
